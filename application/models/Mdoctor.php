<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdoctor extends CI_Model{
	var $tableName = 'doctor';
	var $primaryKey = 'docId';
	var $tableFields = 'docId, DoctorName, DoctorEmail,Doctorpass,DoctorMobile,docCreateDt,fk_branchId,branchName';
	function __construct() {
		parent::__construct();
		//$this->tableName = $tableName;
	} 
	
	public function read($whereArr=array(), $result_by=''){
		$this->db->select($this->tableFields);
		$this->db->from($this->tableName);
		$this->db->join("branch","branch.branchId=$this->tableName.fk_branchId","left");  

		if(!empty($whereArr)){
			foreach($whereArr as $whereArrKey => $whereArrVal){
				if($whereArrVal != null || $whereArrVal != 0)
				$this->db->where($whereArrKey,$whereArrVal);
			}
		}
		$query = $this->db->get();
		if($result_by=='result')
		{
			return $output=$query->result_array();	
		}
		else
		{
			return $output=$query->row_array();	
		}
	}
	

	public function count($whereArr=array(), $result_by=''){
		$this->db->select($this->tableFields);
		$this->db->from($this->tableName);
		$this->db->join("branch","branch.branchId=$this->tableName.fk_branchId","left");  

		if(!empty($whereArr)){
			foreach($whereArr as $whereArrKey => $whereArrVal){
				if($whereArrVal != null || $whereArrVal != 0)
				$this->db->where($whereArrKey,$whereArrVal);
			}
		}
		$query = $this->db->get();
		if($result_by=='result')
		{
			return $output=$query->num_rows();	
		}
		else
		{
			return $output=$query->num_rows();	
		}
	}
	

	
	
}

?>