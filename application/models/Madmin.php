<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Madmin extends CI_Model{
	var $tableName = 'admin';
	var $primaryKey = 'adminId';
	var $tableFields = 'adminId, adminName, adminPassword,createDt,adminEmail,adminMobile';
	function __construct() {
		parent::__construct();
		//$this->tableName = $tableName;
	} 
	
	public function read($whereArr=array(), $result_by=''){
		$this->db->select($this->tableFields);
		$this->db->from($this->tableName);
		if(!empty($whereArr)){
			foreach($whereArr as $whereArrKey => $whereArrVal){
				if($whereArrVal != null || $whereArrVal != 0)
				$this->db->where($whereArrKey,$whereArrVal);
			}
		}
		$query = $this->db->get();
		if($result_by=='result')
		{
			return $output=$query->result_array();	
		}
		else
		{
			return $output=$query->row_array();	
		}
	}
	
	
	
}

?>