<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mcommon extends CI_Model{
	public function master_insert($array,$table_name){
		$this->db->insert($table_name,$array);
		$insert_id=$this->db->insert_id();
		return array('flag'=>1,'emsg'=>'','smsg'=>'Added','insert_id'=>$insert_id);
	}
	public function master_insert_batch($array_insert,$table_name){
		if($array_insert)
		{
			$this->db->insert_batch($table_name,$array_insert);	
		}
	}
	
	public function master_insert_batch_update_on_duplicate($table_name, $array_insert ) {
		if($table_name != '' &&  !empty($array_insert))
		{
			$this->db->on_duplicate($table_name, $array_insert);
		}
	}
	public function master_update_increament1($table_name='',$whereArr=array(),$field='',$count=''){
		if(!empty($whereArr)){
			foreach($whereArr as $whereArrKey => $whereArrVal){
				$this->db->where($whereArrKey,$whereArrVal);
			}
		} 
		$fc = $field.'+'.$count;
		$this->db->set($field, $fc, FALSE);
		$this->db->update($table_name);
	}
	
	public function master_update($table_name,$dataArr=array(),$whereArr=array()){
		if(!empty($whereArr)){
			foreach($whereArr as $whereArrKey => $whereArrVal){
				$this->db->where($whereArrKey,$whereArrVal);
			}
		}
		$this->db->update($table_name,$dataArr);
		unset($whereArr);
		unset($dataArr);
		return array('flag'=>1,'emsg'=>'','smsg'=>'Updated');
	}
	
	public function master_update_increament($table_name,$dataArr=array(),$whereArr=array()){
		
		/* 
			$this->db->where('id', $post['identifier']);
			$this->db->set('votes', 'votes+1', FALSE);
			$this->db->update('users');
		*/
		if(!empty($whereArr)){
			foreach($whereArr as $whereArrKey => $whereArrVal){
				$this->db->where($whereArrKey, $whereArrVal);
			}
		}
		if(!empty($dataArr)){
			foreach($dataArr as $dataArrKey => $dataArrVal){
				$this->db->set($dataArrKey, $dataArrVal, false);
			}
		} 
		
		$this->db->update($table_name);
		unset($whereArr); unset($dataArr);
		return array('flag'=>1,'emsg'=>'','smsg'=>'Updated');
	}
	
	

	public function master_update_batch($table_name,$dataArr=array(),$whereFeild=''){
		$this->db->update_batch($table_name,$dataArr, $whereFeild); 
		return array('flag'=>1,'emsg'=>'','smsg'=>'Updated');
	}
	public function master_delete($array,$table_name){
		$this->db->delete($table_name,$array);
	
		return array('flag'=>1,'emsg'=>'','smsg'=>'deleted');
	}
	public function select_field_result($table_name,$field_names,$whereArr=array()){
		$this->db->select($field_names);
		$this->db->from($table_name);
		if(!empty($whereArr)){
			foreach($whereArr as $whereArrKey => $whereArrVal){
				$this->db->where($whereArrKey,$whereArrVal);
			}
		}
		$query=$this->db->get();
		return $output=$query->result_array();	
	}
	public function select_field_result_single($table_name,$field_names,$whereArr=array()){
		$this->db->select($field_names);
		$this->db->from($table_name);
		if(!empty($whereArr)){
			foreach($whereArr as $whereArrKey => $whereArrVal){
				$this->db->where($whereArrKey,$whereArrVal);
			}
		}
		$query=$this->db->get();
		return $output=$query->row_array();	
	}
	public function categories($whereArr=array(),$result_by=''){
		$this->db->select('categoryId,category,categoryST');
		$this->db->from('categories');
		if(!empty($whereArr)){
			foreach($whereArr as $whereArrKey => $whereArrVal){
				$this->db->where($whereArrKey,$whereArrVal);
			}
		}
		$this->db->order_by('category','ASC');
		$query=$this->db->get();
		if($result_by=='result')
		{
			return $output=$query->result_array();	
		}
		else
		{
			return $output=$query->row_array();	
		}
	}
	public function mst_categories($whereArr=array(),$result_by=''){
		$this->db->select('categoryId,categoryName,categorySlug,categoryParentId,categoryStatus,categoryLogo');
		$this->db->from('mst_categories');
		if(!empty($whereArr)){
			foreach($whereArr as $whereArrKey => $whereArrVal){
				$this->db->where($whereArrKey,$whereArrVal);
			}
		}
		$this->db->order_by('categoryName','ASC');
		$query=$this->db->get();
		if($result_by=='result')
		{
			return $output=$query->result_array();	
		}
		else
		{
			return $output=$query->row_array();	
		}
	}
	public function categories_level_one($whereArr=array(),$result_by=''){
		$this->db->select('categoryOneId,fk_categoryId,categoryOne,categoryOneST');
		$this->db->from('categories_level_one');
		if(!empty($whereArr)){
			foreach($whereArr as $whereArrKey => $whereArrVal){
				$this->db->where($whereArrKey,$whereArrVal);
			}
		}
		$this->db->order_by('categoryOne','ASC');
		$query=$this->db->get();
		if($result_by=='result')
		{
			return $output=$query->result_array();	
		}
		else
		{
			return $output=$query->row_array();	
		}
	}
	public function products($whereArr=array(),$result_by=''){
		$this->db->select('productId,fk_categoryId,productName,productPrice');
		$this->db->from('products');
		if(!empty($whereArr)){
			foreach($whereArr as $whereArrKey => $whereArrVal){
				$this->db->where($whereArrKey,$whereArrVal);
			}
		}
		$this->db->order_by('productName','ASC');
		$query=$this->db->get();
		if($result_by=='result')
		{
			return $output=$query->result_array();	
		}
		else
		{
			return $output=$query->row_array();	
		}
	}
	
	public function mst_cities($whereArr=array(),$result_by=''){
		$this->db->select('cityId,cityName,fk_stateId,cityStatus');
		$this->db->from('mst_cities');
		if(!empty($whereArr)){
			foreach($whereArr as $whereArrKey => $whereArrVal){
				$this->db->where($whereArrKey,$whereArrVal);
			}
		}
		$this->db->order_by('cityName','ASC');
		$query = $this->db->get();
		if($result_by=='result')
		{
			return $output=$query->result_array();	
		}
		else
		{
			return $output=$query->row_array();	
		}
	}
	public function mst_city($whereArr=array(),$result_by=''){
		$this->db->select('cityId,cityName,cityParentId,cityStatus');
		$this->db->from('mst_city');
		if(!empty($whereArr)){
			foreach($whereArr as $whereArrKey => $whereArrVal){
				$this->db->where($whereArrKey,$whereArrVal);
			}
		}
		$this->db->order_by('cityName','ASC');
		$query = $this->db->get();
		if($result_by=='result')
		{
			return $output=$query->result_array();	
		}
		else
		{
			return $output=$query->row_array();	
		}
	}
	public function cityByState($whereArr=array(),$result_by=''){
		$this->db->select("child.cityId as cityId, parent.cityName as stateName, child.cityName as cityName, child.cityParentId as cityParentId");
		$this->db->from("mst_city as parent");
		$this->db->join("mst_city as child","parent.cityId=child.cityParentId","left");
		if(!empty($whereArr)){
			foreach($whereArr as $whereArrKey => $whereArrVal){
				$this->db->where($whereArrKey,$whereArrVal);
			}
		}
		$this->db->order_by("cityName","ASC");
		$query = $this->db->get();
		if($result_by=='result')
		{
			return $output=$query->result_array();	
		}
		else
		{
			return $output=$query->row_array();	
		}
	}
	public function cityByStateAjx($cityName){
		$this->db->select("child.cityId as cityId, parent.cityName as stateName, child.cityName as cityName, child.cityParentId as cityParentId");
		$this->db->from("mst_city as parent");
		$this->db->join("mst_city as child","parent.cityId=child.cityParentId","left");
		$this->db->like("child.cityName",$cityName);
		$this->db->order_by("cityName","ASC");
		$query = $this->db->get();
		return $output=$query->result_array();	
	}
	public function location_by_city($fk_cityId){
		$this->db->select('*');
		$this->db->from('mst_locations');
		$this->db->where('fk_cityId',$fk_cityId);
		$query=$this->db->get();
		return $output=$query->result_array();
	}
	
	public function seller($whereArr=array(),$result_by=''){
		$this->db->select('sellerId,companyName,firstName,lastName,emailId,mobileNo,address,sellerStatus,companyImg,address2,landmark,pincode,phoneNumber,locationName,cityName');
		$this->db->from('seller');
		$this->db->join("mst_locations","mst_locations.locationId=seller.fk_locationId","left");
		$this->db->join("mst_cities","mst_cities.cityId=mst_locations.fk_cityId","left");
		if(!empty($whereArr)){
			foreach($whereArr as $whereArrKey => $whereArrVal){
				$this->db->where($whereArrKey,$whereArrVal);
			}
		}
		$query = $this->db->get();
		if($result_by=='result')
		{
			return $output=$query->result_array();	
		}
		else
		{
			return $output=$query->row_array();	
		}
	}
	public function mst_reqstatus($whereArr=array(),$result_by=''){
		$this->db->select('reqStatusId,reqStatus');
		$this->db->from('mst_reqstatus');
		if(!empty($whereArr)){
			foreach($whereArr as $whereArrKey => $whereArrVal){
				$this->db->where($whereArrKey,$whereArrVal);
			}
		}
		$this->db->order_by('reqStatusId','ASC');
		$query = $this->db->get();
		if($result_by=='result')
		{
			return $output=$query->result_array();	
		}
		else
		{
			return $output=$query->row_array();	
		}
	}
	public function mst_order_status($whereArr=array(),$result_by=''){
		$this->db->select('orderStatusId,orderStatus');
		$this->db->from('mst_order_status');
		if(!empty($whereArr)){
			foreach($whereArr as $whereArrKey => $whereArrVal){
				$this->db->where($whereArrKey,$whereArrVal);
			}
		}
		$this->db->order_by('orderStatusId','ASC');
		$query = $this->db->get();
		if($result_by=='result')
		{
			return $output=$query->result_array();	
		}
		else
		{
			return $output=$query->row_array();	
		}
	}
	public function mst_bid_status($whereArr=array(),$result_by=''){
		$this->db->select('bidStatusId,bidStatus');
		$this->db->from('mst_bid_status');
		if(!empty($whereArr)){
			foreach($whereArr as $whereArrKey => $whereArrVal){
				$this->db->where($whereArrKey,$whereArrVal);
			}
		}
		$this->db->order_by('bidStatusId','ASC');
		$query = $this->db->get();
		if($result_by=='result')
		{
			return $output=$query->result_array();	
		}
		else
		{
			return $output=$query->row_array();	
		}
	}
	public function reviews($whereArr=array(),$result_by='',$orderArr=array(),$page_limit=0,$page_offset=0){
		$this->db->select('reviewId,ratingGiven,revDescription,ratingDT,ratingIP,fk_ratingStatusId,fk_orderId,fk_poId,company_details.companyName as userCompany, users.emailId as userEmail, seller.companyName as sellerCompany, user_seller.emailId as sellerEmail, to_fk_companyId,from_fk_companyId');
		$this->db->from('reviews');
		$this->db->join("company_details","company_details.companyId=reviews.from_fk_companyId","left");
		$this->db->join("users","users.fk_companyId=company_details.companyId","left");
		$this->db->join("company_details seller","seller.companyId=reviews.to_fk_companyId","left");
		$this->db->join("users user_seller","user_seller.fk_companyId=seller.companyId","left");
		if(!empty($whereArr)){
			foreach($whereArr as $whereArrKey => $whereArrVal){
				$this->db->where($whereArrKey,$whereArrVal);
			}
		}
		if(!empty($orderArr)){
			foreach($orderArr as $orderArrKey => $orderArrVal){
				$this->db->order_by($orderArrKey,$orderArrVal);
			}
		}
		if($page_limit)
		{
			$this->db->limit($page_limit,$page_offset);
		}			
		$query=$this->db->get();
		if($result_by=='result')
		{
			return $output=$query->result_array();	
		}
		else
		{
			return $output=$query->row_array();	
		}
	}
	public function off_order_status($whereArr=array(),$result_by=''){
		$this->db->select('orderStatusId,orderStatus');
		$this->db->from('off_order_status');
		if(!empty($whereArr)){
			foreach($whereArr as $whereArrKey => $whereArrVal){
				$this->db->where($whereArrKey,$whereArrVal);
			}
		}
		$this->db->order_by('orderStatusId','ASC');
		$query = $this->db->get();
		if($result_by=='result')
		{
			return $output=$query->result_array();	
		}
		else
		{
			return $output=$query->row_array();	
		}
	}
	public function order_payment($whereArr=array(),$result_by=''){
		$this->db->select('paymentId, fk_orderId, order_payment.fk_companyId, amount_paid, transactionDT, fk_transactionStatusId, txnId, mst_payment_modes.paymentMode as paymentMode, referenceNo, payAddedDT, payStatus, payConfDT, finalPrice,fk_paymentModeId');
		$this->db->from('order_payment');
		$this->db->join("orders","orders.orderId=order_payment.fk_orderId","left");
		$this->db->join("mst_payment_modes","mst_payment_modes.paymentModeId=order_payment.fk_paymentModeId","left");
		if(!empty($whereArr)){
			foreach($whereArr as $whereArrKey => $whereArrVal){
				if($whereArrVal != null || $whereArrVal != 0)
					$this->db->where($whereArrKey,$whereArrVal);
				
			}
		}
		
		$query=$this->db->get();
		if($result_by=='result')
		{
			return $output=$query->result_array();	
		}
		else
		{
			return $output=$query->row_array();	
		}
	}
	public function order_statement($fk_companyId,$fk_orderId){
		$q= $this->db->query("SELECT t1.transHead, t1.fk_paymentId, t1.fk_orderId, t1.transDT, t1.fk_companyId, t1.statementId, t1.fk_transTypeId, mst_trans_types.transType, t1.amountSt, sum(t2.amountSt*case when t2.fk_transTypeId = '2' then 1 else -1 end) as balance FROM order_statement t1 INNER JOIN order_statement t2 ON t1.fk_companyId = t2.fk_companyId AND t1.statementId >= t2.statementId LEFT JOIN mst_trans_types ON mst_trans_types.transTypeId = t1.fk_transTypeId where t1.fk_companyId='$fk_companyId' and t2.fk_companyId='$fk_companyId' and t1.fk_orderId='$fk_orderId' and t2.fk_orderId='$fk_orderId' GROUP BY t1.fk_companyId,t1.statementId,t1.fk_transTypeId,t1.amountSt"
		);
		return $q->result_array();
		
	}
	public function check_sum_payment($fk_orderId){
		$q= $this->db->query("SELECT sum(`amountSt`) as sum FROM `order_statement` WHERE `fk_transTypeId` = 1 and `fk_orderId` = $fk_orderId "
		);
		return $q->row_array();
		
	}
	public function thirdparty_comments($whereArr=array(),$result_by=''){
		$this->db->select('*');
		$this->db->from('thirdparty_comments');

		if(!empty($whereArr)){
			foreach($whereArr as $whereArrKey => $whereArrVal){
				if($whereArrVal != null || $whereArrVal != 0)
					$this->db->where($whereArrKey,$whereArrVal);
				
			}
		}
		
		$query=$this->db->get();
		if($result_by=='result')
		{
			return $output=$query->result_array();	
		}
		else
		{
			return $output=$query->row_array();	
		}
	}
	public function mst_req_cancel_reason(){
		$this->db->select('reqCancelReaonId,reqCancelReaon');
		$this->db->from('mst_req_cancel_reason');
		$this->db->where('reqCancelReasonStatus',1);
		$query=$this->db->get();
		return $output=$query->result_array();	
	}
	public function get_attributes_category($fk_categoryId){
		$this->db->select('attribute');
		$this->db->from('mst_category_attributes_mapped');
		$this->db->join("mst_cateogory_attributes","mst_cateogory_attributes.attributeId=mst_category_attributes_mapped.fk_attributeId","left");
		$this->db->where('fk_categoryId',$fk_categoryId);
		$this->db->where('mapStatus',1);
		$this->db->where('attributeStatus',1);
		$query=$this->db->get();
		return $output=$query->result_array();	
	}
	public function mst_logger_types(){
		$this->db->select('loggerTypeId,loggerType');
		$this->db->from('mst_logger_types');
		$this->db->where('loggerTypeStatus',1);
		$query=$this->db->get();
		return $output=$query->result_array();	
	}
	public function mst_payment_modes(){
		$this->db->select('paymentModeId,paymentMode');
		$this->db->from('mst_payment_modes');
		$this->db->where('paymentModeStatus',1);
		$query=$this->db->get();
		return $output=$query->result_array();	
	}
	public function mst_payment_on(){
		$this->db->select('paymentOnId,paymentOn');
		$this->db->from('mst_payment_on');
		$this->db->where('paymentOnStatus',1);
		$query=$this->db->get();
		return $output=$query->result_array();	
	}
	public function log_po_reject($whereArr=array()){
		$this->db->select('logPORejectId, fk_orderId, fk_userId, fk_companyId, cancelHead, cancelDesc, poRejectDT');
		$this->db->from('log_po_reject');
		if(!empty($whereArr)){
			foreach($whereArr as $whereArrKey => $whereArrVal){
				if($whereArrVal != null || $whereArrVal != 0)
					$this->db->where($whereArrKey,$whereArrVal);
			}
		}
		$query=$this->db->get();
		return $output=$query->row_array();	
	}
	
	public function mst_units($whereArr=array(),$result_by=''){
		$this->db->select('unitId,unitName,unitStatus');
		$this->db->from('mst_units');
		if(!empty($whereArr)){
			foreach($whereArr as $whereArrKey => $whereArrVal){
				$this->db->where($whereArrKey,$whereArrVal);
			}
		}
		$this->db->order_by('unitName','ASC');
		$query = $this->db->get();
		if($result_by=='result')
		{
			return $output=$query->result_array();	
		}
		else
		{
			return $output=$query->row_array();	
		}
	}
	
	public function buyer_seller_terms($whereArr=array(),$result_by=''){
		$this->db->select('termId, terms, fk_companyId, templateName, termsDT, termsStatus');
		$this->db->from('buyer_seller_terms');
		if(!empty($whereArr)){
			foreach($whereArr as $whereArrKey => $whereArrVal){
				$this->db->where($whereArrKey,$whereArrVal);
			}
		}
		$this->db->order_by('templateName','ASC');
		$query = $this->db->get();
		if($result_by=='result')
		{
			return $output=$query->result_array();	
		}
		else
		{
			return $output=$query->row_array();	
		}
	}
	
	
	public function mst_working_flow_steps($whereArr=array(),$result_by=''){
		$this->db->select('stepId, stepName, stepStatus, stepImg');
		$this->db->from('mst_working_flow_steps');
		if(!empty($whereArr)){
			foreach($whereArr as $whereArrKey => $whereArrVal){
				$this->db->where($whereArrKey,$whereArrVal);
			}
		}
		$this->db->order_by('stepId','ASC');
		$query = $this->db->get();
		if($result_by=='result')
		{
			return $output=$query->result_array();	
		}
		else
		{
			return $output=$query->row_array();	
		}
	}
	
	public function order_working_flow($whereArr=array(),$result_by=''){
		$this->db->select('stepId, stepName, stepStatus, stepImg');
		$this->db->from('order_working_flow');
		$this->db->join("mst_working_flow_steps","mst_working_flow_steps.stepId=order_working_flow.fk_stepId","left");
		if(!empty($whereArr)){
			foreach($whereArr as $whereArrKey => $whereArrVal){
				$this->db->where($whereArrKey,$whereArrVal);
			}
		}
		$this->db->order_by('ordeWorkingFlowId','ASC');
		$query = $this->db->get();
		if($result_by=='result')
		{
			return $output=$query->result_array();	
		}
		else
		{
			return $output=$query->row_array();	
		}
	}
	
	public function log_order_status($whereArr=array(),$result_by=''){
		$this->db->select('logOrderStsId, fk_orderId, fk_orderStatusId, buyerCompanyId,sellerCompanyId, orderStatus');
		$this->db->from('log_order_status');
		$this->db->join("mst_order_status","mst_order_status.orderStatusId=log_order_status.fk_orderStatusId","left"); 
		if(!empty($whereArr)){
			foreach($whereArr as $whereArrKey => $whereArrVal){
				$this->db->where($whereArrKey,$whereArrVal);
			}
		}

		$query = $this->db->get();
		if($result_by=='result')
		{
			return $output=$query->result_array();	
		}
		else
		{
			return $output=$query->row_array();	
		}
	}


	public function buyer_seller_product_map_read($whereArr=array(),$result_by=''){
		$this->db->select('fk_invBuyerProductId,fk_invSellerProductId,fk_sellerId');
		$this->db->from('buyer_seller_product_map');
		if(!empty($whereArr)){
			foreach($whereArr as $whereArrKey => $whereArrVal){
				$this->db->where($whereArrKey,$whereArrVal);
			}
		}
		$query=$this->db->get();
		if($result_by=='result')
		{
			return $output=$query->result_array();	
		}
		else
		{
			return $output=$query->row_array();	
		}
	}

	public function table_columns($tableName){
		$SQL = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '".$this->db->database."' AND TABLE_NAME  = '".$tableName."'";
		$query = $this->db->query($SQL);

		return $query->result_array();
	}
	
	public function table_autoincreament_id($tableName){
		$SQL = "SELECT AUTO_INCREMENT FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = '".$this->db->database."' AND TABLE_NAME  = '".$tableName."'";
		$query = $this->db->query($SQL);

		return $query->row_array();
	}
	 
	
}

?>