<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminlogin extends CI_Controller {

	var $data = array();

	public function __construct(){
       parent::__construct();		
		$models = array('mcommon', 'madmin','mbranch','mdoctor','mstaff');
		foreach($models as $model){
			$this->load->model($model);
		}
		$this->load->library('common_functions');
	}


	public function index()
	{

		if($this->input->is_ajax_request())
		{
			
			$this->form_validation->set_error_delimiters('', '');
			$this->form_validation->set_rules('Username', 'User Name', 'required|alpha|trim');
			$this->form_validation->set_rules('password', 'Password', 'required');
			if($this->form_validation->run() == FALSE)
			{
				$output=array('flag'=>3,'Username'=>form_error('Username'),'password'=>form_error('password'),'smsg'=>'');
				echo json_encode($output);
			}
			else
			{
				$username= $this->input->post('Username');
				$password= $this->input->post('password');

				$users = $this->madmin->read(array('adminName'=>$username,'adminPassword'=>$password),'row');
				if($users)
				{

				$session_array = array('adminName'=>$users['adminName'],'adminPassword'=>$users['adminPassword']);	
				$this->session->set_userdata('session_array',$session_array);

				$output=array('flag'=>1,'smsg'=>'login Suceess','redirect'=>site_url('adminlogin/add_staff'));
				}else
				{

				$output=array('flag'=>2,'smsg'=>'','esmg'=>'User not exist','redirect'=>'');

				}

				echo json_encode($output);			
			}	
		}
		else
		{
			$this->load->view('login');
		}
	}

	public function logout()
	{
	$this->session->sess_destroy();
	redirect(base_url());
	}

	

	public function add_admin()
	{

		if($this->input->is_ajax_request())
		{
			
			$this->form_validation->set_error_delimiters('', '');
			$this->form_validation->set_rules('adminName', 'Admin Name', 'required|trim');
			$this->form_validation->set_rules('adminPassword', 'admin Password', 'required|trim');
			$this->form_validation->set_rules('adminEmail', 'Admin Email', 'required|valid_email|trim');
			$this->form_validation->set_rules('adminMobile', 'Admin Mobile', 'required|exact_length[10]|trim');

			if($this->form_validation->run() == FALSE)
			{
				$output=array('flag'=>3,'adminName'=>form_error('adminName'),'adminPassword'=>form_error('adminPassword'),'staffpass'=>form_error('staffpass'),'adminMobile'=>form_error('adminMobile'),'smsg'=>'');
				echo json_encode($output);
			}
			else
			{
				$adminName= $this->input->post('adminName');
				$adminPassword= $this->input->post('adminPassword');
				$adminEmail= $this->input->post('adminEmail');
				$adminMobile = $this->input->post('adminMobile');
				$password = sha1($adminPassword);


				$admin = $this->madmin->read(array('adminEmail'=>$adminEmail,'adminMobile'=>$adminMobile));
				if($admin)
				{

					$output=array('flag'=>2,'smsg'=>'','emsg'=>'Already Exist Admin','redirect'=>1);
				}else
				{


					$insert_array  = array('adminPassword' =>$password ,'adminEmail'=>$adminEmail,'adminPassword'=>$adminPassword,'adminName'=>$adminName,'adminMobile'=>$adminMobile );

					$this->mcommon->master_insert($insert_array,'admin');
					$output=array('flag'=>1,'smsg'=>'Admin Added Suceessfully','esmg'=>'','redirect'=>1);

				}

				echo json_encode($output);			
			}	
		}
		else
		{
			$this->data['branch'] = $this->mbranch->read(array(),'result');
			$this->load->view('add_staff',$this->data);
		}
	}


	public function add_staff()
	{

		if($this->input->is_ajax_request())
		{
			
			$this->form_validation->set_error_delimiters('', '');
			$this->form_validation->set_rules('staffName', 'Staff Name', 'required|trim');
			$this->form_validation->set_rules('staffEmail', 'staff Email', 'required|valid_email|trim');
			$this->form_validation->set_rules('staffpass', 'staffpass', 'required|trim');
			$this->form_validation->set_rules('staffMobile', 'staffMobile', 'required|exact_length[10]|trim');

			if($this->form_validation->run() == FALSE)
			{
				$output=array('flag'=>3,'staffName'=>form_error('staffName'),'staffEmail'=>form_error('staffEmail'),'staffpass'=>form_error('staffpass'),'staffMobile'=>form_error('staffMobile'),'smsg'=>'');
				echo json_encode($output);
			}
			else
			{
				$staffName= $this->input->post('staffName');
				$staffEmail= $this->input->post('staffEmail');
				$staffpass= $this->input->post('staffpass');
				$staffMobile = $this->input->post('staffMobile');
				$password = sha1($staffpass);


				$staff = $this->mstaff->read(array('staffEmail'=>$staffEmail,'staffMobile'=>$staffMobile));
				if($staff)
				{

					$output=array('flag'=>2,'smsg'=>'','emsg'=>'Already Exist Staff','redirect'=>1);
				}else
				{


					$insert_array  = array('staffpass' =>$password ,'staffEmail'=>$staffEmail,'staffName'=>$staffName,'staffMobile'=>$staffMobile );

					$this->mcommon->master_insert($insert_array,'staff');
					$output=array('flag'=>1,'smsg'=>'Staff Added Suceessfully','esmg'=>'','redirect'=>1);

				}

				echo json_encode($output);			
			}	
		}
		else
		{
			$this->data['branch'] = $this->mbranch->read(array(),'result');
			$this->load->view('add_staff',$this->data);
		}
	}


	public function add_doctor()
	{

		if($this->input->is_ajax_request())
		{
			
			$this->form_validation->set_error_delimiters('', '');
			$this->form_validation->set_rules('DoctorName', 'Doctor Name', 'required|trim');
			$this->form_validation->set_rules('DoctorEmail', 'Doctor Email', 'required|valid_email|trim');
			$this->form_validation->set_rules('Doctorpass', 'Doctorpass', 'required|trim');
			$this->form_validation->set_rules('DoctorMobile', 'DoctorMobile', 'required|exact_length[10]|trim');
			$this->form_validation->set_rules('fk_branchId', 'fk_branchId', 'required|numeric|trim');

			if($this->form_validation->run() == FALSE)
			{
				$output=array('flag'=>3,'DoctorName'=>form_error('DoctorName'),'DoctorEmail'=>form_error('DoctorEmail'),'Doctorpass'=>form_error('Doctorpass'),'DoctorMobile'=>form_error('DoctorMobile'),'fk_branchId'=>form_error('fk_branchId'),'smsg'=>'');
				echo json_encode($output);
			}
			else
			{
				$DoctorName= $this->input->post('DoctorName');
				$DoctorEmail= $this->input->post('DoctorEmail');
				$Doctorpass= $this->input->post('Doctorpass');
				$DoctorMobile = $this->input->post('DoctorMobile');
				$fk_branchId = $this->input->post('fk_branchId');
				$password = sha1($Doctorpass);


				$Doctor = $this->mdoctor->read(array('DoctorEmail'=>$DoctorEmail,'DoctorMobile'=>$DoctorMobile));
				if($Doctor)
				{

					$output=array('flag'=>2,'smsg'=>'','emsg'=>'Already Exist Doctor','redirect'=>1);
				}else
				{


					$insert_array  = array('Doctorpass' =>$password ,'DoctorEmail'=>$DoctorEmail,'DoctorName'=>$DoctorName,'DoctorMobile'=>$DoctorMobile,'fk_branchId'=>$fk_branchId );

					$this->mcommon->master_insert($insert_array,'doctor');
					$output=array('flag'=>1,'smsg'=>'Doctor Added Suceessfully','esmg'=>'','redirect'=>1);

				}

				echo json_encode($output);			
			}	
		}
		else
		{
			$this->data['branch'] = $this->mbranch->read(array(),'result');
			$this->load->view('add_staff',$this->data);
		}
	}

	public function show_list_staff()
	{
		$this->data['show_list_staff'] = $this->mstaff->read(array(),'result');
		$this->load->view('show_list_staff',$this->data);

	}

	public function show_list_doctor()
	{
		$intructorId= $this->uri->segment(3);
		$this->data['show_list_doctor'] = $this->mdoctor->read(array(),'result');
		$this->load->view('show_list_doctor',$this->data);
	}


	public function show_list_admin()
	{
		$intructorId= $this->uri->segment(3);
		$this->data['show_list_admin'] = $this->madmin->read(array(),'result');
		$this->load->view('show_list_admin',$this->data);
	}


	public function dashboard()
	{
		$intructorId= $this->uri->segment(3);
		$this->data['doctor_count'] = $this->mdoctor->count(array(),'result');
		$this->data['staff_count'] = $this->mstaff->count(array(),'result');
		$this->load->view('dashboard',$this->data);
	}
























}
