<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Doctorlogin extends CI_Controller {

	var $data = array();

	public function __construct(){
       parent::__construct();		
		$models = array('mcommon', 'madmin','mbranch','mdoctor','mstaff','mpatient');
		foreach($models as $model){
			$this->load->model($model);
		}
		$this->load->library('common_functions');
	}


	public function index()
	{

		if($this->input->is_ajax_request())
		{
			
			$this->form_validation->set_error_delimiters('', '');
			$this->form_validation->set_rules('DoctorEmail', 'Doctor  Email', 'required|valid_email|trim');
			$this->form_validation->set_rules('password', 'Password', 'required');
			if($this->form_validation->run() == FALSE)
			{
				$output=array('flag'=>3,'DoctorEmail'=>form_error('DoctorEmail'),'password'=>form_error('password'),'smsg'=>'');
				echo json_encode($output);
			}
			else
			{
				$DoctorEmail= $this->input->post('DoctorEmail');
				$passwor = $this->input->post('password');
				$password = sha1($passwor);


				$users = $this->mdoctor->read(array('DoctorEmail'=>$DoctorEmail,'Doctorpass'=>$password),'row');
				if($users)
				{

				$session_array = array('adminName'=>$users['DoctorEmail'],'adminPassword'=>$users['Doctorpass']);	
				$this->session->set_userdata('staff_session_array',$session_array);

				$output=array('flag'=>1,'smsg'=>'login Suceess','redirect'=>site_url('doctorlogin/dashboard'));
				}else
				{

				$output=array('flag'=>2,'smsg'=>'','esmg'=>'Doctor not exist','redirect'=>'');

				}

				echo json_encode($output);			
			}	
		}
		else
		{
			$this->load->view('doctorlogin');
		}
	}

	public function logout()
	{
	$this->session->sess_destroy('staff_session_array');
	redirect(base_url());
	}


	public function add_patient()
	{

		if($this->input->is_ajax_request())
		{
			
			$this->form_validation->set_error_delimiters('', '');
			$this->form_validation->set_rules('patientName', 'Patient Name', 'required|alpha|trim');
			$this->form_validation->set_rules('patientMobile', 'Patient Mobile', 'required|numeric|trim');
			$this->form_validation->set_rules('age', 'age', 'required|trim');

			if($this->form_validation->run() == FALSE)
			{
				$output=array('flag'=>3,'patientName'=>form_error('patientName'),'patientMobile'=>form_error('patientMobile'),'age'=>form_error('age'),'smsg'=>'');
				echo json_encode($output);
			}
			else
			{
				$patientName= $this->input->post('patientName');
				$patientMobile= $this->input->post('patientMobile');
				$age= $this->input->post('age');
				$password = sha1($staffpass);

					$insert_array  = array('patientName' =>$patientName ,'patientMobile'=>$patientMobile,'age'=>$age,'fk_roleId'=>$fk_roleId);
					$this->mcommon->master_insert($insert_array,'patient');
					$output=array('flag'=>1,'smsg'=>'Patient Added Suceessfully','esmg'=>'','redirect'=>1);

				echo json_encode($output);			
			}	
		}
		else
		{
			$this->data['branch'] = $this->mbranch->read(array(),'result');
			$this->load->view('add_staff',$this->data);
		}
	}

	

	public function show_list_patient()
	{
		$this->data['show_list_patient'] = $this->mpatient->read(array(),'result');
		$this->load->view('show_list_patient',$this->data);

	}


























}
