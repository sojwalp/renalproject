<?php $this->load->view('header');?>
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:<?php echo base_url()?>partials/_sidebar.html -->
        <?php $this->load->view('sidebar');?>
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="row">

              <div class="col-md-6 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Add Staff</h4>
                    <form class="common_function" method="post" action="<?php echo base_url()?>adminlogin/add_staff">
                    
                     <input type='hidden' name='input_error_classes' value='staffName,staffEmail,staffpass,staffMobile'>
                    <input type='hidden' name='error_class' value='emsg'>
                    <input type='hidden' name='success_class' value='smsg'>


                      <div class="form-group">
                        <label for="exampleInputPassword4"> Staff Name</label>
                        <input type="text" class="form-control" id="" name="staffName" placeholder="Staff Name">
                        <p class="text-danger staffName"></p>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword4"> Staff Email</label>
                        <input type="text" class="form-control" id="" name="staffEmail" placeholder="Staff Email">
                        <p class="text-danger staffEmail"></p>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword4"> Staff Password</label>
                        <input type="password" class="form-control" id="" name="staffpass" placeholder="Staff Password">
                        <p class="text-danger staffpass"></p>
                      </div>
                    
                      <div class="form-group">
                        <label for="exampleInputPassword4"> Staff Name</label>
                        <input type="text" class="form-control" id="" name="staffMobile" maxlength="10" placeholder="Ex:8999989989">
                        <p class="text-danger staffMobile"></p>
                      </div>
                      

                      <button type="submit" class="btn btn-success mr-2">Submit</button>
                      <button class="btn btn-light" type="reset">Cancel</button>

                       <p class='text-center'>
                      <span class="text-danger emsg"></span>
                      <span class="text-success smsg"></span>
                      </p>

                    </form>
                  </div>
                </div>
              </div>
              <div class="col-md-6 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Add Doctor</h4>
                    <form class="common_function" method="post" action="<?php echo base_url()?>adminlogin/add_doctor">
                    
                     <input type='hidden' name='input_error_classes' value='DoctorName,DoctorEmail,Doctorpass,DoctorMobile'>
                    <input type='hidden' name='error_class' value='emsg'>
                    <input type='hidden' name='success_class' value='smsg'>


                      <div class="form-group">
                        <label for="exampleInputPassword4"> Doctor Name</label>
                        <input type="text" class="form-control" id="" name="DoctorName" placeholder="Doctor Name">
                        <p class="text-danger DoctorName"></p>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword4"> Doctor Email</label>
                        <input type="text" class="form-control" id="" name="DoctorEmail" placeholder="Doctor Email">
                        <p class="text-danger DoctorEmail"></p>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword4"> Doctor Password</label>
                        <input type="password" class="form-control" id="" name="Doctorpass" placeholder="Doctor Password">
                        <p class="text-danger Doctorpass"></p>
                      </div>
                    
                      <div class="form-group">
                        <label for="exampleInputPassword4"> Doctor Mobile </label>
                        <input type="text" class="form-control" id="" name="DoctorMobile" maxlength="10" placeholder="Ex:8999989989">
                        <p class="text-danger DoctorMobile"></p>
                      </div>

                      <div class="form-group">
                        <label for="exampleInputPassword4">Select Branch</label>
                        <select class="form-control" name="fk_branchId">
                        <option value="0">Select Branch</option>
                        <?php
                        if($branch)
                        {
                          foreach ($branch as $key => $value) {
                        ?>
                        <option value="<?php echo $value['branchId'];?>"><?php echo $value['branchName'];?></option>
                        <?php
                        }}
                        ?>
                        </select>
                        <p class="text-danger fk_branchId"></p>
                      </div>

                      
                      

                      <button type="submit" class="btn btn-success mr-2">Submit</button>
                      <button class="btn btn-light" type="reset">Cancel</button>

                       <p class='text-center'>
                      <span class="text-danger emsg"></span>
                      <span class="text-success smsg"></span>
                      </p>

                    </form>
                  </div>
                </div>
              </div>
              
            </div>

            <div class="row">
              <div class="col-md-6 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Add Admin</h4>
                    <form class="common_function" method="post" action="<?php echo base_url()?>adminlogin/add_admin">
                    
                     <input type='hidden' name='input_error_classes' value='adminName,adminEmail,adminPassword,adminMobile'>
                    <input type='hidden' name='error_class' value='emsg'>
                    <input type='hidden' name='success_class' value='smsg'>


                      <div class="form-group">
                        <label for="exampleInputPassword4"> Admin Name</label>
                        <input type="text" class="form-control" id="" name="adminName" placeholder="Admin Name">
                        <p class="text-danger adminName"></p>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword4"> Admin Email</label>
                        <input type="text" class="form-control" id="" name="adminEmail" placeholder="Admin Email">
                        <p class="text-danger adminEmail"></p>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword4"> Admin Password</label>
                        <input type="password" class="form-control" id="" name="adminPassword" placeholder="Admin Password">
                        <p class="text-danger adminPassword"></p>
                      </div>
                    
                      <div class="form-group">
                        <label for="exampleInputPassword4"> Admin Mobile</label>
                        <input type="text" class="form-control" id="" name="adminMobile" maxlength="10" placeholder="Ex:8999989989">
                        <p class="text-danger adminMobile"></p>
                      </div>
                      

                      <button type="submit" class="btn btn-success mr-2">Submit</button>
                      <button class="btn btn-light" type="reset">Cancel</button>

                       <p class='text-center'>
                      <span class="text-danger emsg"></span>
                      <span class="text-success smsg"></span>
                      </p>

                    </form>
                  </div>
                </div>
              </div>
              
            </div>
          </div>
         <?php $this->load->view('footer');?>