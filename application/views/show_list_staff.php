<?php $this->load->view('header');?>
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:<?php echo base_url()?>partials/_sidebar.html -->
        <?php $this->load->view('sidebar');?>
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="row">

              <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Staff List</h4>
                    <table class="table">
                      <thead>
                        <tr>
                          <th>Staff Name</th>
                          <th>Staff Mobile.</th>
                          <th>Staff Email</th>
                          <th>Join Date</th>
                        </tr>
                      </thead>
                      <tbody>
                    <?php
                   if($show_list_staff){
                    foreach ($show_list_staff as $key => $value) {
                   ?>


                          <tr>
                          <td><?php echo $value['staffName'];?></td>
                          <td><?php echo $value['staffMobile'];?></td>
                          <td><?php echo $value['staffEmail'];?></td>
                          <td>
                            <?php echo $value['staffcreateDt'];?>
                          </td>
                        </tr>

                   <?php }}else{ echo "No Data Found";}?>
                   </tbody>
                 </table>
                  </div>
                </div>
              </div>
              
            </div>
          </div>
         <?php $this->load->view('footer');?>