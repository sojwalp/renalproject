<?php $this->load->view('header');?>
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:<?php echo base_url()?>partials/_sidebar.html -->
        <?php $this->load->view('sidebar');?>
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="row">

              <div class="col-md-6 grid-margin stretch-card">
                    <div class="card">
                      <div class="card-body">
                        <div class="row">
                          <div class="col-md-6">
                            <div class="d-flex align-items-center pb-2">
                              <div class="dot-indicator bg-danger mr-2"></div>
                              <p class="mb-0">Active Staff</p>
                            </div>
                            <h4 class="font-weight-semibold"><?php echo $staff_count;?></h4>
                            <div class="progress progress-md">
                              <div class="progress-bar bg-danger" role="progressbar" style="width: <?php echo $staff_count;?>%" aria-valuenow="<?php echo $staff_count;?>" aria-valuemin="0" aria-valuemax="<?php echo $staff_count;?>"></div>
                            </div>
                          </div>
                          <div class="col-md-6 mt-4 mt-md-0">
                            <div class="d-flex align-items-center pb-2">
                              <div class="dot-indicator bg-success mr-2"></div>
                              <p class="mb-0">Active Doctor</p>
                            </div>
                            <h4 class="font-weight-semibold"><?php echo $doctor_count;?></h4>
                            <div class="progress progress-md">
                              <div class="progress-bar bg-success" role="progressbar" style="width: <?php echo $doctor_count;?>%" aria-valuenow="<?php echo $doctor_count;?>" aria-valuemin="0" aria-valuemax="<?php echo $doctor_count;?>"></div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
            </div>
          </div>
         <?php $this->load->view('footer');?>