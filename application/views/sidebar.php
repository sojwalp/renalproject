<nav class="sidebar sidebar-offcanvas" id="sidebar">
          <ul class="nav">
            <li class="nav-item nav-profile">
              <a href="#" class="nav-link">
                <div class="profile-image">
                  <img class="img-xs rounded-circle" src="<?php echo base_url()?>assets/images/faces/face8.jpg" alt="profile image">
                  <div class="dot-indicator bg-success"></div>
                </div>
                <div class="text-wrapper">
                  <p class="profile-name"><?php $sessdata= $this->session->userdata('session_array'); ?></p>
                  Welcome <?php echo ucfirst($sessdata['adminName']); ?>
                </div>
              </a>
            </li>
            <li class="nav-item nav-category">Main Menu</li>
            <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url()?>adminlogin/dashboard">
                <i class="menu-icon typcn typcn-document-text"></i>
                <span class="menu-title">Dashboard</span>
              </a>
            </li>
            
            <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url()?>adminlogin/show_list_staff">
                <i class="menu-icon typcn typcn-shopping-bag"></i>
                <span class="menu-title">List Staff</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url()?>adminlogin/show_list_doctor">
                <i class="menu-icon typcn typcn-shopping-bag"></i>
                <span class="menu-title">List Doctor</span>
              </a>
            </li>

            <li class="nav-item">
              <a class="nav-link" href="<?php echo base_url()?>adminlogin/show_list_admin">
                <i class="menu-icon typcn typcn-shopping-bag"></i>
                <span class="menu-title">List Admin</span>
              </a>
            </li>

            
          </ul>
        </nav>