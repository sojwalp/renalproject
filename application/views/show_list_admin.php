<?php $this->load->view('header');?>
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:<?php echo base_url()?>partials/_sidebar.html -->
        <?php $this->load->view('sidebar');?>
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="row">

              <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">

                    <h4 class="card-title">Admin List</h4>
                    <table class="table">
                      <thead>
                        <tr>
                          <th>Admin Name</th>
                          <th>Admin Mobile.</th>
                          <th>Admin Email</th>
                          <th>Join Date</th>
                        </tr>
                      </thead>
                      <tbody>
                    <?php
                   if($show_list_admin){
                    foreach ($show_list_admin as $key => $value) {
                   ?>


                          <tr>
                          <td><?php echo $value['adminName'];?></td>
                          <td><?php echo $value['adminMobile'];?></td>
                          <td><?php echo $value['adminEmail'];?></td>
                          <td><?php echo $value['createDt'];?></td>
                        </tr>

                   <?php }}else{ echo "No Data Found";}?>
                   </tbody>
                 </table>
                  </div>
                </div>
              </div>
              
            </div>
          </div>
         <?php $this->load->view('footer');?>