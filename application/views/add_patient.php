<?php $this->load->view('header');?>
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <?php $this->load->view('sidebar');?>
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="row">

              <div class="col-md-6 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">
                    <h4 class="card-title">Add Patient</h4>
                    <form class="common_function" method="post" action="<?php echo base_url()?>stafflogin/add_patient">
                    
                     <input type='hidden' name='input_error_classes' value='patientName,patientMobile,age'>
                    <input type='hidden' name='error_class' value='emsg'>
                    <input type='hidden' name='success_class' value='smsg'>


                      <div class="form-group">
                        <label for="exampleInputPassword4"> Patient Name</label>
                        <input type="text" class="form-control" id="" name="patientName" placeholder="Patient Name">
                        <p class="text-danger patientName"></p>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword4"> Patient Phone</label>
                        <input type="text" class="form-control" id="" name="patientMobile" maxlength="10" placeholder="Ex:7888878887">
                        <p class="text-danger patientMobile"></p>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword4"> Patient Age</label>
                        <input type="text" class="form-control" id="" name="age" placeholder="Ex:24">
                        <p class="text-danger age"></p>
                      </div>
                      
                      <button type="submit" class="btn btn-success mr-2">Submit</button>
                      <button class="btn btn-light" type="reset">Cancel</button>

                       <p class='text-center'>
                      <span class="text-danger emsg"></span>
                      <span class="text-success smsg"></span>
                      </p>

                    </form>
                  </div>
                </div>
              </div>
              
            </div>

          </div>
         <?php $this->load->view('footer');?>