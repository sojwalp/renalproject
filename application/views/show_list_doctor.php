<?php $this->load->view('header');?>
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:<?php echo base_url()?>partials/_sidebar.html -->
        <?php $this->load->view('sidebar');?>
        <!-- partial -->
        <div class="main-panel">
          <div class="content-wrapper">
            <div class="row">

              <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                  <div class="card-body">

                    <h4 class="card-title">Doctor List</h4>
                    <table class="table">
                      <thead>
                        <tr>
                          <th>Doctor Name</th>
                          <th>Doctor Mobile.</th>
                          <th>Doctor Email</th>
                          <th>Branch</th>
                          <th>Join Date</th>
                        </tr>
                      </thead>
                      <tbody>
                    <?php
                   if($show_list_doctor){
                    foreach ($show_list_doctor as $key => $value) {
                   ?>


                          <tr>
                          <td><?php echo $value['DoctorName'];?></td>
                          <td><?php echo $value['DoctorMobile'];?></td>
                          <td><?php echo $value['DoctorEmail'];?></td>
                          <td><?php echo ucfirst($value['branchName']);?></td>
                          <td><?php echo $value['docCreateDt'];?></td>
                        </tr>

                   <?php }}else{ echo "No Data Found";}?>
                   </tbody>
                 </table>
                  </div>
                </div>
              </div>
              
            </div>
          </div>
         <?php $this->load->view('footer');?>